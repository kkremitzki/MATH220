\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}The K\IeC {\"o}nigsberg Bridge Problem}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Introductory Definitions}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Euler's Abstraction}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Key Observation: Degree of a Vertex}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Conclusion to the Problem}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Food webs: an application of graph theory.}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Introduction}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Graph theoretic approach}{8}{subsection.3.2}
\contentsline {section}{\numberline {4}References}{10}{section.4}
\contentsline {section}{\numberline {5}Figures}{11}{section.5}
