\documentclass{article}
\linespread{2}

% Packages
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{sidecap}
\usepackage{wrapfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{tabulary}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{graphicx}
\usepackage{svg}
\usepackage{framed}
\usepackage{caption}
\usepackage{hyperref}
\usepackage[autostyle]{csquotes}
\usepackage{amsthm}


% Custom
\newcommand{\threeast}{\bigskip\par\centerline{*\,*\,*}\medskip\par}%
\newcommand{\triast}{\bigskip\par\noindent\parbox{\linewidth}{\centering\large{*}\\[-4pt]{*}\hskip 0.75em{*}}\bigskip\par}%
\newcommand{\source}[1]{\caption*{Source: {#1}} }
\newcommand{\repeatcaption}[2]{%
  \renewcommand{\thefigure}{\ref{#1}}%
  \captionsetup{list=no}%
  \caption{#2 (repeated from page \pageref{#1})}%
}


\begin{document}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{Page \thepage\ of \pageref{LastPage}}
\pagenumbering{arabic}
\begin{titlepage}
 \vspace*{\stretch{1.0}}
   \begin{center}
     \Large\textbf{\#13. An impossible walk: the seven bridges of Königsberg and graph theory.}\\
     \large\textit{Kurt W. Kremitzki}\\
     \large\textit{MATH220-904 Writing Assignment Final Paper}
   \end{center}
\begin{abstract}
Leonhard Euler pioneered the field of graph theory in the early 18th century when he investigated what is known as the Königsberg bridge problem.
In this paper, we explore the technique of transformation from concrete to abstract utilized by Euler in classifying and solving the problem.
By showing that a path which crosses each of the seven bridges only once is impossible, Euler started a rich and fundamental line of inquiry in math.
\end{abstract}

   \vspace*{\stretch{2.0}}
\end{titlepage}

\clearpage

\tableofcontents
\listoffigures
\clearpage

\section{Introduction}
In 1735, the Prussian city of Königsberg straddled both banks of the river Pregel and featured a famous landmark where, at one point in the river, two islands were connected to the mainland and each other by a total of seven bridges.
This landmark was the origin of the puzzle known as the Seven Bridges of Königsberg, and can be seen in Fig. \ref{fig:bridges} on page \pageref{fig:bridges}. The point of the game
is to cross each bridge exactly once and return to one's origin. The details of the puzzle have been lost with time, but it is often claimed without proof that it was a popular pastime for 
the burghers of Königsberg to begin the day at the Kneiphof, or pub, boasting that they could complete the walk and return victorious. 
Upon their return as failures, consolation could of course only be found in a mug of beer \cite{mallion}. 
\par
Popular opinion then was that the feat was impossible, but the matter would not be settled until being addressed by the monumental mathematician Leonhard Euler.
On August 26, 1735, he presented a mathematical demonstration of the impossibility of the problem to the Petersburg Academy. 
The Latin title translated to English is ``\textit{The solution to a problem relating to the geometry of position}'', a reference to a class of problems described by Leibniz.
In his paper, Euler said of ``the geometry of position'' that 
\blockquote{this branch is concerned only with the determination of position and its properties; it does not involve distances, nor calculations made with them.}
In contrast, Euler said in a previous letter that the problem
\blockquote{bears little relationship to mathematics ... for the solution is based on reason alone, and its
discovery does not depend on any mathematical principle.}
\par
In retrospect, Euler's remarks make sense when we consider his investigation as the beginning of a new, unique branch of mathematics---graph theory.
\cite{ams}

\section{The Königsberg Bridge Problem}

\subsection{Introductory Definitions}
Our inquiry begins by retracing Euler's steps in solving the problem, using the modern definitions of graph theory, which is rooted in set theory \cite{dover}.
\theoremstyle{definition}
\newtheorem{defn}{Definition}
\newtheorem{ex}{Example}
\newtheorem*{thm}{Theorem}

  \begin{defn}{\cite[p.19]{dover}} (Graph): A \textit{graph} is an object consisting of two sets called its vertex set and edge set. 
    The vertex set is a finite nonempty set. The edge set may be empty, but otherwise its elements are two-element subsets of the vertex set.
  \end{defn}

  \begin{ex}The sets 
  \begin{equation}\{A, B, C\}\end{equation} and 
  \begin{equation}\{\{A, B\}, \{A, C\}, \{B, C\}\}\end{equation} 
  are the vertex set and edge set, respectively, of the graph shown in Fig. \ref{fig:simple} on page \pageref{fig:simple}.
  \end{ex}

  \begin{defn}{\cite[p.19]{dover}} (Vertex): The elements of the vertex set are called \textit{vertices} (singular: \textit{vertex}). The term \textit{node} is synonymous with vertex.
  \end{defn}

  \begin{defn}{\cite[p.20]{dover}} (Edge): The elements of the edge set are called \textit{edges}. If $\{X, Y\}$ is an edge of a graph with vertices $X$ and $Y$,
    we say that $\{X, Y\}$ joins or connects the vertices $X$ and $Y$, and $X$ and $Y$ are said to be adjacent; $X$ and $Y$ are said to be incident to the edge $\{X, Y\}$.
  \end{defn}

  \begin{defn}(Euler path): 
    A sequence of edges where no edge is repeated and every edge in a graph is used exactly once is called an \textit{Euler path} or \textit{Euler circuit}..
  \end{defn}

\subsection{Euler's Abstraction}
Examining the definition of an Euler path, we see that the solution of the Königsberg bridge problem lies in determining if there exists some physical walk taken by the 
burghers of the city can be represented by a graph that has an Euler path.
\par
However, Euler did not have such a convenient definition at hand when he first encountered this problem. 
At first, he considered the possibility of listing out all possible paths and examining them to see if one satisfies the conditions of the problem.
Of course, such a method of exhaustion would be tedious, if it were to work at all. 
If some city in the future were to have a more complicated set of bridges and islands, it might take more than a lifetime to list out all possible routes.
\par
Recognizing the futility of this approach, Euler sought to abstract the situation. He realized that, in essence, the islands and regions of the city could be reduced to simple points.
In turn, the bridges that connected those regions could be drawn as lines between points; all that mattered was which points were connected to each other. 
Lengths and areas of the city or its bridges were irrelevant.
\par
Again, turning to our definition, we see that the points and the lines connecting them correspond to our notion of vertices and edges of a graph.
In Fig. \ref{fig:graph} on page \pageref{fig:graph}, a \textit{graph drawing} for the Königsberg bridge problem is shown. 
Note the emphasis on the term ``graph drawing'': the image in the figure is a representation of the graph, of which there are many. 
Lines can be lengthened, stretched, or points rearranged, so long as the list of vertices and the list of edges connecting them is preserved.
\par
To summarize key insights about the graph drawing, note that we have a graph with 4 vertices; of the 4 vertices, one is incident to five edges, while the other three are incident to three edges each.

\begin{figure}[ht]
  \begin{framed}
  \centering
    \includegraphics[scale=0.4]{graph}
  \end{framed}
  \repeatcaption{fig:graph}{Graph drawing.}
\end{figure}

\subsection{Key Observation: Degree of a Vertex}
The key insight to the solution of this problem is this: if we are ``walking'' on a graph, the number of edges connected to a vertex matters.
\begin{defn} (Degree): The \textit{degree} of a vertex is the number of incident edges on the vertex.
\end{defn}

For any vertex with an even number of incident edges, or $2n, n\in\mathbb{Z}$ edges, we can leave $n$ times and return $n$ times.
\par
However, if the vertex has an odd degree, it has $2n + 1$ incident edges, so if we enter $n$ times, we must leave $n + 1$ times, or vice versa.
\par
Obviously, when taking a path around a graph, we must begin on one vertex and end on another vertex. Now, armed with our definition of the degree of a vertex, and our observation about odd nodes,
this seemingly trivial fact about the starting and ending vertex leads to a surprisingly conclusion.

\subsection{Conclusion to the Problem}
Consider the graph drawing shown in Fig. \ref{fig:odd} on page \pageref{fig:odd}. This graph drawing has an Euler path beginning on one node and ending on the other;
as a consequence of having two nodes with odd degree, one must be the starting node and the other must be the ending node. If the graph was a trivial case having 0 nodes of odd degree,
each node could be entered $n$ times and left $n$ times, also making an Euler path.
\par
However, these are the only two times in which a graph with nodes of odd degree can have an Euler path; without proof, we give the following theorem which is the result of Euler's work:
\begin{thm}
  If G is a graph with 0 or 2 vertices of odd degree, then G contains an Euler path.
\end{thm}

Any higher number of odd-degree vertices results in a graph that does not have an Euler path. 
To see why this is true, recall that a vertex with an odd number of incident edges must be the starting point or end point, since it must be entered 1 time more than it is left, or vice versa.
The presence of odd-degree vertices imposes a restriction on whether a graph has an Euler path or not; since only one vertex can be the starting point and only one can be the ending point, 
either 0 or 2 odd-degree vertices are allowable.
\par
Recall that in Fig. \ref{fig:graph}, our graph drawing has 4 nodes, with 3 of them having degree 3 and the other node having degree 5. Since all 4 nodes have odd degree, and $4 > 2$, the graph does not have
an Euler path. Therefore, it is impossible to walk the seven bridges of Königsberg while crossing each bridge only once.
\par
The publishing of Euler's result was a foundational moment for graph theory, although it hardly seemed mathematical to him at the time \cite{ams}. He reduced
a complex, physical problem that took a whole day to attempt into one that could be solved with only pencil and paper; 
his proof of the impossibility of walking the bridges was a moment where the essence of mathematics was revealed.

\section{Food webs: an application of graph theory.}
\subsection{Introduction}
Ecology tackles problems of incredible complexity and thus calls upon any branch of math that will yield insight. 
One such topic is that of food webs, which is a way of describing connections between species; essentially who eats what. 
Accordingly, they are sometimes called consumer-resource systems. Such systems in view of graph theory are impossibly complex
since the list of nodes would be the millions and millions of species to have ever existed, and the list of vertices is any predator-prey interaction
since the beginning of life on Earth. Through a thermodynamic accounting of energy flows, this problem can be viewed in a more general light, which may
yield profound insights. If fundamental pathways exist across many or all food webs, we could gain understanding of the movement of mass and energy on
massive scales, the nature of species interdependence and reaction to perturbation in a chaotic system based on fundamental principles revealed by our inquiry.
\cite{webs}

\subsection{Graph theoretic approach}
Food webs can be idealized as graphs by adding properties. First, consider an ecosystem such as the African savannah, one of the most productive on Earth.
The huge amount of energy captured from sunlight by the savannah grass serves as the basis of many food webs, and thus we can assign a weighting factor
based on the rate of energy or mass flow. Then, considering food webs describe consumption, we can assign a direction to vertices, where one node eats or otherwise
is connected to another.

When many food webs are examined this way, we can see that nodes which contain relatively few or no inward flows or outward flows are likely sources or sinks.
In other words, they represent species that receive or transmit matter or energy to or from outside the system. 
From there, individual food webs can be abstracted as nodes themselves in a graph of connected food webs, and from their connectedness we can gain further insight.

For example, if we were to connect food webs of producers, consumers, and decomposers, we may end up with a connected graph, where, for example, energy or mass flow from a
savannah grass species ends up being consumed, decomposed, and returned to it. 
Then, the number of nodes that make up this cycle and the acyclic paths available from those nodes can represent some measure of importance
of that species to the ecosystem.

Obviously, graph theory has incredible explanatory power. Beyond a mere thought experiment or silly game, this mathematical study of connectedness sheds light on subtle
yet fundamental mechanics of nature, while at the same time is a perfectly abstract branch of pure math.

% References
\clearpage
\section{References}
\begin{thebibliography}{9}

  \bibitem{mallion}
    Mallion, Roger. "A contemporary Eulerian walk over the bridges of Kaliningrad." BSHM Bulletin 23, no. 1 (2008): 24-36.

  \bibitem{dover}
    Richard J. Trudeau, Introduction to Graph Theory, Dover Publications, Inc., New York, NY, 1993.

  \bibitem{ams}
    Gerald L. Alexanderson, Euler and Königsberg's Bridges: A Historical View, \textit{Bulletin of the American Mathematical Society}, Volume 43, Number 4, October 2006, Pages 567–573.
    Available online at \url{http://www.ams.org/journals/bull/2006-43-04/S0273-0979-06-01130-X/S0273-0979-06-01130-X.pdf}

  \bibitem{bridges}
    Wikimedia Commons. File:Königsberg bridges.png. Available online at \url{https://commons.wikimedia.org/wiki/File:Konigsberg_bridges.png}

  \bibitem{graph}
    Wikimedia Commons. File:Königsberg graph.svg. Available online at \url{https://commons.wikimedia.org/wiki/File:K%C3%B6nigsberg_graph.svg}

  \bibitem{webs}
  Allesina, Stefano, Antonio Bodini, and Cristina Bondavalli. "Ecological subsystems via graph theory: the role of strongly connected components." Oikos 110.1 (2005): 164-176.
\end{thebibliography}

% Figures
\clearpage
\begin{samepage}
\section{Figures}

\begin{figure}[ht]
  \begin{framed}
  \caption{The seven bridges of Königsberg.}
  \label{fig:bridges}
  \source{Image by Bogdan Giuşcă. / CC BY-SA \cite{bridges}}
  \centering
    \includegraphics[scale=0.9]{bridges}
  \end{framed}
\end{figure}

\begin{figure}[ht]
  \begin{framed}
  \caption{Graph drawing of the seven bridges.}
  \label{fig:graph}
  \source{Image by ``en:User:Booyabazooka''. / CC BY-SA \cite{graph}}
  \centering
    \includegraphics[scale=0.4]{graph}
  \end{framed}
\end{figure}


\begin{figure}[ht]
  \begin{framed}
  \caption{A simple graph with 3 vertices and 3 edges.}
  \label{fig:simple}
  \source{Image by Kurt Kremitzki.}
  \centering
    \includegraphics[scale=0.5]{simple_graph}
  \end{framed}
\end{figure}


\begin{figure}[ht]
  \begin{framed}
  \caption{A graph with 2 nodes of odd degree.}
  \label{fig:odd}
  \source{Image by Kurt Kremitzki.}
  \centering
    \includegraphics[scale=0.6]{odd}
  \end{framed}
\end{figure}

\end{samepage}
\end{document}
